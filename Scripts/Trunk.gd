extends Node2D

onready var sprite = $Sprite
onready var leftAxe = $LeftAxe
onready var rightAxe = $RightAxe
onready var timer = $Timer

var speed = 2000
var direction = 1
var  spriteHeight

func _ready():
	spriteHeight = sprite.texture.get_height() * scale.y
	set_process(false)

func _process(delta):
	position.x += speed * direction * delta

func InitializeTrunk(axe, right):
	if axe:
		if right:
			leftAxe.queue_free()
		else:
			rightAxe.queue_free()
	else:
		leftAxe.queue_free()
		rightAxe.queue_free()

func Remove(fromRight):
	if fromRight:
		direction = -1
	else:
		direction = 1
	timer.start()
	set_process(true)

func _on_Timer_timeout():
	queue_free()
