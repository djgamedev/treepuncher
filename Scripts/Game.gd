extends Node

export(PackedScene) var trunkScene
onready var trunkPosition = $FirstTrunkPosition
onready var timeLeft = $TimeLeft
onready var player = $Player
onready var grave = $Grave
onready var timer = $Timer
onready var scoreText = $ScoreText

var lastSpawnPosition
var trunks = []
var lastHasAxe = false
var lastAxeRight = false
var isDead = false
var score

func _ready():
	score = 0
	lastSpawnPosition = trunkPosition.position
	spawnFirstTrunks()
	pass

func _process(delta):
	if isDead:
		return
	timeLeft.value -= delta
	if timeLeft.value <= 0:
		Die()

func AddTrunk(axe, axe_right):
	var newTrunk = trunkScene.instance()
	add_child(newTrunk)
	newTrunk.position = lastSpawnPosition
	newTrunk.InitializeTrunk(axe, axe_right)
	trunks.append(newTrunk)

func spawnFirstTrunks():
	for i in range(5):
		var newTrunk = trunkScene.instance()
		add_child(newTrunk)
		newTrunk.position = lastSpawnPosition
		lastSpawnPosition.y -= newTrunk.spriteHeight
		newTrunk.InitializeTrunk(false, false)
		trunks.append(newTrunk)

func PunchTree(trunkRight):
	if !lastHasAxe:
		if rand_range(0, 100) > 50:
			lastAxeRight = rand_range(0, 100) > 50
			lastHasAxe = true
		else:
			lastHasAxe = false
	else:
		if rand_range(0, 100) > 50:
			lastHasAxe = true
		else:
			lastHasAxe = false
	AddTrunk(lastHasAxe, lastAxeRight)
	
	trunks[0].Remove(trunkRight)
	trunks.pop_front()
	for trunk in trunks:
		trunk.position.y += trunk.spriteHeight
	
	timeLeft.value += 0.25
	if timeLeft.value >= timeLeft.max_value:
		timeLeft.value = timeLeft.max_value
	
	score += 1
	scoreText.text = "Score: " + str(score)

func Die():
	grave.position.x = player.position.x
	player.queue_free()
	grave.visible = true
	timer.start()
	isDead = true

func _on_Timer_timeout():
	get_tree().reload_current_scene()
